from django.contrib import admin

# Register your models here.

from .models import (Administrateur, ContactUs, Entreprise, Souvenirs,
					Stagiaire, Projets, NotionsAcquises, Rapports, StageContexte,
					Newsletter, Employé, Faq, Stat)


admin.site.register(Administrateur)
admin.site.register(Entreprise)
admin.site.register(Souvenirs)
admin.site.register(Stat)
admin.site.register(Employé)
admin.site.register(Stagiaire)
admin.site.register(Projets)
admin.site.register(NotionsAcquises)
admin.site.register(Rapports)
admin.site.register(StageContexte)
admin.site.register(Newsletter)
admin.site.register(Faq)
admin.site.register(ContactUs)