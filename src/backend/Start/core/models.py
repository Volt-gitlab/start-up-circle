from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin

from datetime import datetime, date


class Administrateur(models.Model):

	user = models.OneToOneField(User, on_delete=models.CASCADE)
	photo_profil = models.ImageField(default='default_ad.png', upload_to='media', blank=True)

	class Meta:
		verbose_name_plural = 'Administrateurs'

	def __str__(self):
		return "admin - " + self.user.username


class Entreprise(models.Model):

	nom = models.CharField(max_length=255)
	contact = models.IntegerField()
	second_contact = models.IntegerField(blank=True, null=True, default=None)
	localisation = models.CharField(max_length=255)
	domaine_application = models.CharField(max_length=255)
	logo = models.ImageField(default='default_logo.png', upload_to='media', blank=True)
	date_enregistrement = models.DateTimeField(auto_now=True)
	administrateur = models.OneToOneField(Administrateur, null=True, on_delete=models.DO_NOTHING)

	def clean(self):
		if self.second_contact is None:
			self.second_contact = 0

	class Meta:
		verbose_name_plural = 'Entreprises'

	def __str__(self):
		return self.nom + " - " + self.domaine


class Souvenirs(models.Model):

	description = models.TextField()
	photo = models.ImageField()
	date_ajout = models.DateTimeField(auto_now=True)
	user = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING) 
	entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE)

	class Meta:
		verbose_name_plural = 'Souvenirs'

	def __str__(self):
		return "Souvenir de " + self.entreprise.nom + " publié par " + self.user.username


class Projets(models.Model):
	
	nom = models.CharField(max_length=255)
	description = models.TextField()	
	date_enregistrement = models.DateTimeField(default=datetime.now, blank=True)
	choisi = models.BooleanField(default=False)
	user = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING) 
	entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE) 

	class Meta:
		verbose_name_plural = 'Projets'

	def __str__(self):
		return self.nom + " proposé par " + self.user.username + " de l'entreprise " + self.entreprise.nom


class StageContexte(models.Model):

	date_debut = models.DateTimeField()
	date_fin = models.DateTimeField()
	domaine = models.CharField(max_length=255)
	niveau_requis = models.CharField(max_length=255)
	description = models.TextField()
	lieu_stage = models.CharField(max_length=255)
	date_creation = models.DateTimeField(auto_now=True)
	entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE)

	class Meta:
		verbose_name_plural = 'Stages'

	def __str__(self):
		return self.domaine + " - " + self.entreprise.nom


class Stagiaire(models.Model):

	SEXES = [('Feminin', 'Feminin'), ('Masculin', 'Masculin')]

	user = models.OneToOneField(User, on_delete=models.CASCADE)
	date_naissance = models.DateField()
	lieu_naissance = models.CharField(max_length=255)
	sexe = models.CharField(max_length=10, choices=SEXES)
	contact = models.IntegerField()
	photo_profil = models.ImageField(default='default_st.png', upload_to='media', blank=True)
	date_arrivee = models.DateTimeField(auto_now=True)
	est_renvoyé = models.BooleanField(default=False)

	# juste pour garder les stagiaires meme ci on supprime l'entreprise
	entreprise = models.ForeignKey(Entreprise,  null = True, on_delete=models.SET_NULL) 
	stage = models.ManyToManyField(StageContexte)

	class Meta:
		verbose_name_plural = 'Stagiaires'

	def __str__(self):
		return "stagiaire - " + self.user.username


class Employé(models.Model):

	SEXES = [('Feminin', 'Feminin'), ('Masculin', 'Masculin')]

	user = models.OneToOneField(User, on_delete=models.CASCADE)
	sexe = models.CharField(max_length=10, choices=SEXES)
	contact = models.IntegerField()
	photo_profil = models.ImageField(default='default_st.png', upload_to='media', blank=True)
	date_arrivee = models.DateTimeField(auto_now=True)
	est_renvoyé = models.BooleanField(default=False)
	cv = models.FileField()
	poste = models.CharField(max_length = 255)
	residence = models.CharField(max_length = 255)
	entreprise = models.ForeignKey(Entreprise,  null = True, on_delete=models.SET_NULL) 
	
	class Meta:
		verbose_name_plural = 'Employé'

	def __str__(self):
		return "Employé - " + self.user.username


class Stat(models.Model):

	salaire = models.IntegerField()
	entreprise = models.ForeignKey(Entreprise, null= True, on_delete=models.SET_NULL)
	employé = models.ForeignKey(Employé, null = True, on_delete = models.SET_NULL)

	class Meta:
		verbose_name_plural = 'Statistiques'

	def __str__(self):
		return self.user.salaire


class NotionsAcquises(models.Model):
	
	libele = models.CharField(max_length=255)
	description = models.TextField()
	date_ajout = models.DateTimeField(auto_now=True)
	entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE)
	stagiaire = models.ManyToManyField(Stagiaire)

	class Meta:
		verbose_name_plural = 'NotionsAcquises'

	def __str__(self):
		return self.libele + " - " + self.entreprise.nom


class Rapports(models.Model):
	
	titre = models.CharField(max_length=255)
	date_ajout = models.DateTimeField(auto_now=True)
	fichier = models.FileField()
	stagiaire = models.ForeignKey(Stagiaire, on_delete=models.CASCADE)

	class Meta:
		verbose_name_plural = 'Rapports'

	def __str__(self):
		return self.stagiaire.user.username + " - " + self.titre


class Newsletter(models.Model):
	
	email = models.CharField(max_length=255)

	class Meta:
		verbose_name_plural = 'Nos fans'

	def __str__(self):
		return self.email


class ContactUs(models.Model):
	
	name = models.CharField(max_length=255)
	email = models.CharField(max_length=255)
	message = models.TextField(max_length=255)

	class Meta:
		verbose_name_plural = 'Nos contacts'

	def __str__(self):
		return self.name + " - " + self.email


class Faq(models.Model):
	
	question = models.CharField(max_length=255)
	reponse = models.CharField(max_length=255)

	class Meta:
		verbose_name_plural = 'Foire aux questions'
	
	def __str__(self):
		return self.question[:20] + "..."