# Generated by Django 4.0.2 on 2022-02-10 09:58

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Administrateur',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo_profil', models.ImageField(blank=True, default='default_ad.png', upload_to='media')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Administrateurs',
            },
        ),
        migrations.CreateModel(
            name='ContactUs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=255)),
                ('message', models.TextField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Nos contacts',
            },
        ),
        migrations.CreateModel(
            name='Employé',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sexe', models.CharField(choices=[('Feminin', 'Feminin'), ('Masculin', 'Masculin')], max_length=10)),
                ('contact', models.IntegerField()),
                ('photo_profil', models.ImageField(blank=True, default='default_st.png', upload_to='media')),
                ('date_arrivee', models.DateTimeField(auto_now=True)),
                ('est_renvoyé', models.BooleanField(default=False)),
                ('cv', models.FileField(upload_to='')),
                ('poste', models.CharField(max_length=255)),
                ('residence', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Employé',
            },
        ),
        migrations.CreateModel(
            name='Entreprise',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=255)),
                ('contact', models.IntegerField()),
                ('second_contact', models.IntegerField(blank=True, default=None, null=True)),
                ('localisation', models.CharField(max_length=255)),
                ('domaine_application', models.CharField(max_length=255)),
                ('logo', models.ImageField(blank=True, default='default_logo.png', upload_to='media')),
                ('date_enregistrement', models.DateTimeField(auto_now=True)),
                ('administrateur', models.OneToOneField(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='core.administrateur')),
            ],
            options={
                'verbose_name_plural': 'Entreprises',
            },
        ),
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=255)),
                ('reponse', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Foire aux questions',
            },
        ),
        migrations.CreateModel(
            name='Newsletter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Nos fans',
            },
        ),
        migrations.CreateModel(
            name='StageContexte',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_debut', models.DateTimeField()),
                ('date_fin', models.DateTimeField()),
                ('domaine', models.CharField(max_length=255)),
                ('niveau_requis', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('lieu_stage', models.CharField(max_length=255)),
                ('date_creation', models.DateTimeField(auto_now=True)),
                ('entreprise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.entreprise')),
            ],
            options={
                'verbose_name_plural': 'Stages',
            },
        ),
        migrations.CreateModel(
            name='Stat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('salaire', models.IntegerField()),
                ('employé', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.employé')),
                ('entreprise', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.entreprise')),
            ],
            options={
                'verbose_name_plural': 'Statistiques',
            },
        ),
        migrations.CreateModel(
            name='Stagiaire',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_naissance', models.DateField()),
                ('lieu_naissance', models.CharField(max_length=255)),
                ('sexe', models.CharField(choices=[('Feminin', 'Feminin'), ('Masculin', 'Masculin')], max_length=10)),
                ('contact', models.IntegerField()),
                ('photo_profil', models.ImageField(blank=True, default='default_st.png', upload_to='media')),
                ('date_arrivee', models.DateTimeField(auto_now=True)),
                ('est_renvoyé', models.BooleanField(default=False)),
                ('entreprise', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.entreprise')),
                ('stage', models.ManyToManyField(to='core.StageContexte')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Stagiaires',
            },
        ),
        migrations.CreateModel(
            name='Souvenirs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('photo', models.ImageField(upload_to='')),
                ('date_ajout', models.DateTimeField(auto_now=True)),
                ('entreprise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.entreprise')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Souvenirs',
            },
        ),
        migrations.CreateModel(
            name='Rapports',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=255)),
                ('date_ajout', models.DateTimeField(auto_now=True)),
                ('fichier', models.FileField(upload_to='')),
                ('stagiaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.stagiaire')),
            ],
            options={
                'verbose_name_plural': 'Rapports',
            },
        ),
        migrations.CreateModel(
            name='Projets',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('date_enregistrement', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('choisi', models.BooleanField(default=False)),
                ('entreprise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.entreprise')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Projets',
            },
        ),
        migrations.CreateModel(
            name='NotionsAcquises',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libele', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('date_ajout', models.DateTimeField(auto_now=True)),
                ('entreprise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.entreprise')),
                ('stagiaire', models.ManyToManyField(to='core.Stagiaire')),
            ],
            options={
                'verbose_name_plural': 'NotionsAcquises',
            },
        ),
        migrations.AddField(
            model_name='employé',
            name='entreprise',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.entreprise'),
        ),
        migrations.AddField(
            model_name='employé',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
